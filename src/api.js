const url = 'http://localhost/host'

function getHosts() {
  return fetch(`${url}`)
    .then(res => res.json())
    .catch(err => console.log(err))
}

function getHost(host) {
  return fetch(`${url}/${host}`)
    .then(res => res.json())
    .catch(err => console.log(err))
}

export default {
  getHosts,
  getHost
}
